# qFlipper

qFlipper is a desktop application for updating Flipper Zero firmware.

This Ansible role installs qFlipper AppImage of either a specified version or
the latest available version. Tested on Ubuntu 22.04.


## XDG desktop entry

This role uses the `xdg_desktop_entries` and `xdg_desktop_icons` variables,
which are defined in the playbook `group_vars`:

```
xdg_desktop_entries: "{{ xdg_data_dir }}/applications"
xdg_desktop_icons: "{{ xdg_data_dir }}/icons"
```

with `xdg_data_dir` in turn defined inside this role.


## First connection attempt: "invalid device" (resolved)

The GUI says "invalid device" and "device cannot be recognized".
The full log:
```
515 [APP] qFlipper version 1.3.2 commit 7b0839e1 2023-06-08T12:58:22
515 [APP] OS info: Ubuntu 22.04.2 LTS 22.04 5.19.0-45-generic Qt 6.4.3
1047 [UPD] Fetched update information from https://update.flipperzero.one/qFlipper/directory.json
1047 [UPD] Fetched update information from https://update.flipperzero.one/firmware/directory.json
47258 [USB] Failed to open device: Access denied (insufficient permissions)
47278 [USB] Failed to open device: Access denied (insufficient permissions)
47298 [USB] Failed to open device: Access denied (insufficient permissions)
47318 [USB] Failed to open device: Access denied (insufficient permissions)
47339 [USB] Failed to open device: Access denied (insufficient permissions)
47359 [USB] Failed to open device: Access denied (insufficient permissions)
47379 [USB] Failed to open device: Access denied (insufficient permissions)
47399 [USB] Failed to open device: Access denied (insufficient permissions)
47419 [USB] Failed to open device: Access denied (insufficient permissions)
47440 [USB] Failed to open device: Access denied (insufficient permissions)
47460 [USB] Failed to open device: Access denied (insufficient permissions)
47480 [USB] Failed to open device: Access denied (insufficient permissions)
47500 [USB] Failed to open device: Access denied (insufficient permissions)
47521 [USB] Failed to open device: Access denied (insufficient permissions)
47541 [USB] Failed to open device: Access denied (insufficient permissions)
47561 [USB] Failed to open device: Access denied (insufficient permissions)
47581 [USB] Failed to open device: Access denied (insufficient permissions)
47602 [USB] Failed to open device: Access denied (insufficient permissions)
47622 [USB] Failed to open device: Access denied (insufficient permissions)
47642 [USB] Failed to open device: Access denied (insufficient permissions)
47662 [REG] Incomplete device info: VID_0x0:PID_0x0
65231 [qt.qml.context] qrc:/components/TextBox.qml:39:9 Parameter "link" is not declared. Injection of parameters into signal handlers is deprecated. Use JavaScript functions with formal parameters instead.
```

seems to indicate a lack of permission.
Since we are running the AppImage as our regular user, that sounds like something
we should address.

+ https://github.com/flipperdevices/qFlipper/issues/42#issuecomment-1065858903
+ https://github.com/flipperdevices/qFlipper/blob/dev/setup_rules.sh

Other users report that running the above script (which updates udev rules) resolves
this problem. This makes sense.
But rather than running the bash script itself, I think I can easily convert
its simple configuration to ansiblified udev rules (see `tasks/config.yml`).

Ok, the udev rules resolved this issue.



## References

+ https://flipperzero.one/update
+ https://github.com/flipperdevices/qFlipper
